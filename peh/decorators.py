import importlib
from functools import wraps
from peh import ExceptionHandler


def with_handler(*func, group=None, handler=None):
    """
    핸들러 wrapping decorator.
    이 decorator 가 지정된 함수는 예외 발생시 지정된 핸들러를 호출한다.

    decorator 설정은 파라미터 없이 할 수 있으며, 파라미터를 통해 특정 핸들러를 지정할 수 있다.
    예)
        @with_handler
        def test_function():
            pass

        @with_handler(handler=my_handler):
        def test_function():
            pass

    :param func: 핸들링 타켓 함수
    :param group: 핸들러 그룹
    :param handler: 핸들러 함수 - 핸들러 함수를 지정합니다. 함수 포인터 또는 풀 네임의 문자열로 지정 할 수 있습니다.
    :return:
    """
    def wrapper(target):
        @wraps(target)
        def exe(*args, **kwargs):
            try:
                return target(*args, **kwargs)
            except Exception as e:
                inner_handler = handler
                if handler and type(handler) == str:
                    mod = importlib.import_module(handler[:handler.rfind('.')])
                    inner_handler = getattr(mod, handler[handler.rfind('.')+1:])
                return ExceptionHandler.process(
                    e, *args, group=group or target.__module__, handler=inner_handler, **kwargs
                )

        return exe

    if func:
        return wrapper(func[0])
    else:
        return wrapper


def exception_handler(exceptions, group=None):
    def register(func):
        itr = []
        if hasattr(exceptions, '__iter__'):
            itr.extend(exceptions)
        else:
            itr.append(exceptions)

        for e in itr:
            ExceptionHandler.context.handler(e, group=group or func.__module__, handler=func)

    return register
