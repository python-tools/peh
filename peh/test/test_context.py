import unittest

from peh import ExceptionHandler
from peh.test.exceptions import *


class ExceptionHandlerTest(unittest.TestCase):
    message = 'This is a handler for test.'

    def setUp(self):
        self.context = ExceptionHandler.context.__class__()

    def test_register_handler(self):
        def test_handler():
            return self.message

        group = test_handler.__module__
        self.context.handler(TestException, group=group, handler=test_handler)

        handler = self.context.handler(TestException, group=group)

        self.assertEqual(test_handler, handler)
        self.assertEqual(handler(), self.message)

    def test_handler_with_group(self):
        def my_handler():
            return self.message

        self.context.handler(Exception, group='my.a', handler=lambda a : 'this is a handler')
        self.context.handler(TestException, group='my.b', handler=lambda b : 'this is b handler')
        self.context.handler(TestException, group='my.c', handler=lambda c : 'this is c handler')
        self.context.handler(TestException, group='my.c.sample', handler=my_handler)

        handler = self.context.handler(TestException, group='my.c.sample.handler')

        self.assertEqual(my_handler, handler)
        self.assertEqual(handler(), self.message)

    def test_handler_with_multi_inheritance_exception(self):
        class BSub(BFailure):
            pass

        class YSub(YFailure):
            pass

        self.context.handler(AFailure, handler=lambda: 'A type')
        self.context.handler(BFailure, handler=lambda: 'B type')
        self.context.handler(CFailure, handler=lambda: 'C type')
        self.context.handler(DFailure, handler=lambda: 'D type')
        self.context.handler(EFailure, handler=lambda: 'E type')
        self.context.handler(WFailure, handler=lambda: 'W type')
        self.context.handler(XFailure, handler=lambda: 'X type')
        self.context.handler(YFailure, handler=lambda: 'Y type')
        self.context.handler(ZFailure, handler=lambda: 'Z type')
        self.context.handler(AZFailure, handler=lambda: 'AZ type')
        self.context.handler(CZFailure, handler=lambda: 'CZ type')
        self.context.handler(DXFailure, handler=lambda: 'DX type')

        a_handler = self.context.handler(AFailure)
        b_handler = self.context.handler(BFailure)
        c_handler = self.context.handler(CFailure)
        d_handler = self.context.handler(DFailure)
        e_handler = self.context.handler(EFailure)
        w_handler = self.context.handler(WFailure)
        x_handler = self.context.handler(XFailure)
        y_handler = self.context.handler(YFailure)
        z_handler = self.context.handler(ZFailure)
        az_handler = self.context.handler(AZFailure)
        cz_handler = self.context.handler(CZFailure)
        dx_handler = self.context.handler(DXFailure)

        b_sub_handler = self.context.handler(BSub)
        y_sub_handler = self.context.handler(YSub)

        self.assertEqual('A type', a_handler())
        self.assertEqual('B type', b_handler())
        self.assertEqual('C type', c_handler())
        self.assertEqual('D type', d_handler())
        self.assertEqual('E type', e_handler())
        self.assertEqual('W type', w_handler())
        self.assertEqual('X type', x_handler())
        self.assertEqual('Y type', y_handler())
        self.assertEqual('Z type', z_handler())
        self.assertEqual('AZ type', az_handler())
        self.assertEqual('CZ type', cz_handler())
        self.assertEqual('DX type', dx_handler())
        self.assertEqual(b_handler, b_sub_handler)
        self.assertEqual(y_handler, y_sub_handler)

    def test_default_handler(self):
        def test_handler():
            return self.message

        self.context.handler(TestException, handler=test_handler)

        handler = self.context.handler(TestException)

        self.assertEqual(test_handler, handler)
        self.assertEqual(handler(), self.message)

    def test_register_exception(self):
        class InnerException(Exception):
            pass

        self.context.register_exception(TestException, InnerException)

        converted = self.context.convert(TestException)

        self.assertEqual(converted, InnerException)


class TestException(Exception):
    pass
