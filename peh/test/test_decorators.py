import unittest

from peh.decorators import with_handler, exception_handler
from peh.test.supporters import mock_decorator


class DecoratorTest(unittest.TestCase):
    def test_기본_핸들러_decorator(self):
        @with_handler
        def target_function(message):
            raise TestException(message)

        msg = target_function('test message')
        self.assertEqual('call handler (test message)', msg)

    def test_decorator_그룹지정(self):
        @with_handler(group='test.decorator')
        def target_function(message):
            raise TestException(message)

        msg = target_function('test message')
        self.assertEqual('this is test.decorator handler : test message', msg)

    def test_decorator_핸들러지정(self):
        @with_handler(handler=local_handler)
        def target_function(message):
            raise TestException

        msg = target_function('test message')
        self.assertEqual('local handler : test message', msg)

    def test_decorator_문자열로_핸들러지정(self):
        @with_handler(handler='peh.test.test_decorators.local_handler')
        def target_function(message):
            raise TestException

        msg = target_function('test message')
        self.assertEqual('local handler : test message', msg)

    def test_중첩된_decorator(self):
        """
        중첩된 decorator 지정을 할 경우 wrapping 된 decorator 로부터 반환 받은 함수때문에 핸들러의 그룹 정보가 달라지게 된다.
        따라서 중첩된 decorator 로 핸들러를 지정할 경우 기본 핸들러를 사용할 것이 아니라면 타겟을 지정해줘야 한다.

        :return:
        """

        @with_handler(group='test.decorator')
        @mock_decorator
        def target_function(message):
            raise TestException(message)

        msg = target_function('overlapped decorator')
        self.assertEqual('this is test.decorator handler : overlapped decorator', msg)


class TestException(Exception):
    pass


class SampleError(Exception):
    pass


@exception_handler([TestException, SampleError])
def handler(message, e: Exception):
    return 'call handler (%s)' % message


@exception_handler(TestException, group='test.decorator')
def dec_handler(message, e: Exception):
    return 'this is test.decorator handler : %s' % message


def local_handler(message, e: Exception):
    return 'local handler : %s' % message

