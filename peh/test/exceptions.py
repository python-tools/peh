class AFailure(Exception): pass


class BFailure(AFailure): pass


class CFailure(BFailure): pass


class DFailure(CFailure): pass


class EFailure(AFailure): pass


class ZFailure(Exception): pass


class YFailure(ZFailure): pass


class XFailure(YFailure): pass


class WFailure(ZFailure): pass


class AZFailure(AFailure, ZFailure): pass


class CZFailure(CFailure, ZFailure): pass


class DXFailure(DFailure, XFailure): pass