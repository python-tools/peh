def mock_decorator(func):
    def f(*args, **kwargs):
        return func(*args, **kwargs)
    return f
