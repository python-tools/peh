import unittest
from peh import ExceptionHandler
from peh.decorators import with_handler


class ExceptionHandlerTest(unittest.TestCase):
    def setUp(self):
        ExceptionHandler.context.register_exception(ValueError, TestException)

    def test_handler_for_converting_exception(self):
        @with_handler
        def f():
            raise ValueError('sample value error.')

        with self.assertRaises(TestException):
            f()

    def test_handler_for_throwing_source(self):
        @with_handler
        def f():
            raise RuntimeError('runtime error.')

        with self.assertRaises(RuntimeError):
            f()


class TestException(Exception):
    pass