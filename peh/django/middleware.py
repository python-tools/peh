import sys
from peh import ExceptionHandler


def _load_object(name):
    idx = name.rfind('.')
    module = name[:idx]
    cls = name[idx+1:]

    __import__(module)
    m = sys.modules[module]

    return getattr(m, cls)


class ExceptionHandlingMiddleware(object):
    EXCEPTION_CONVERTS = 'EXCEPTION_CONVERTS'
    EXCEPTION_HANDLERS = 'EXCEPTION_HANDLERS'

    def __init__(self):
        settings = _load_object('django.conf.settings')

        self.__register_exceptions(settings)
        self.__register_handlers(settings)

    def __register_exceptions(self, settings):
        if hasattr(settings, self.EXCEPTION_CONVERTS) and settings.EXCEPTION_CONVERTS:
            setting_list = settings.EXCEPTION_CONVERTS
        else:
            setting_list = []

        exceptions = []
        for e in setting_list:
            exceptions.append((_load_object(e[0]), _load_object(e[1])))

        ExceptionHandler.register(exceptions)

    def __register_handlers(self, settings):
        if hasattr(settings, self.EXCEPTION_HANDLERS) and settings.EXCEPTION_HANDLERS:
            for item in settings.EXCEPTION_HANDLERS:
                for k in item:
                    ExceptionHandler.context.handler(exception=_load_object(k), handler=_load_object(item[k]))

    def process_exception(self, request, exception):
        result = ExceptionHandler.process(exception, request)
        return result
