import inspect


def _create_exception_handler_context_():
    class Context(object):
        __slots__ = ['__handler_groups__', '__exception_map__']
        DEFAULT_HANDLER_GROUP = '_base_exception_handlers_'

        def __init__(self):
            """
            핸들러들은 그룹별로 등록된다. 핸들러 그룹은 예외 클래스 별로 등록된 핸들러 목록들을 포함하고 있다.
            핸들러는 최상위 예외 클래스를 식별자로 하여 해당 예외 타입의 핸들러 목록들을 구성한다.

            핸들러 그룹의 자료구조는 다음과 같다.
            {
                '그룹이름': {
                    '최상위 예외클래스': [('예외 클래스', '핸들러'), ('예외 클래스', '핸들러'), ...]
                },
                ....
            }
            """
            self.__handler_groups__ = {self.DEFAULT_HANDLER_GROUP: {}}
            self.__exception_map__ = {}

        def register_exception(self, from_, to_):
            self.__exception_map__[from_] = to_

        def convert(self, exception):
            for e in self.__exception_map__:
                if issubclass(exception, e):
                    return self.__exception_map__[e]

            return exception

        def handler(self, exception, group=None, handler=None):
            if not group:
                group = self.DEFAULT_HANDLER_GROUP

            if handler:
                self.__add_handler__(exception, group, handler)
            else:
                return self.__find_handler__(exception, group)

        def __add_handler__(self, exception, group, handler):
            def order_handlers(handlers):

                return sorted(
                    handlers,
                    key=lambda i: len(inspect.getmro(i[0])),
                    reverse=True
                )

            if group in self.__handler_groups__ and self.__handler_groups__[group]:
                handler_group = self.__handler_groups__[group]
            else:
                handler_group = {}

            work = {exception: [(exception, handler), ]}
            delete_current_key = False
            for key, h in handler_group.copy().items():
                if issubclass(exception, key):
                    work[key] = order_handlers(list(h) + work[exception])
                    delete_current_key = True
                elif issubclass(key, exception):
                    work[exception] = order_handlers(work[exception] + list(h))
                    del handler_group[key]

            if delete_current_key:
                del work[exception]
            handler_group.update(work)
            self.__handler_groups__.update({group: handler_group})

        def __find_handler__(self, exception, group):
            groups = sorted(list(filter(lambda k: group.find(k) == 0, self.__handler_groups__)), reverse=True)

            if not groups:
                groups.append(self.DEFAULT_HANDLER_GROUP)

            for handler_group in [self.__handler_groups__[g] for g in groups]:
                for key, handlers in handler_group.items():
                    if issubclass(exception, key):
                        handler = [h for k, h in handlers if issubclass(exception, k)][0]
                        return handler

            return None

    return Context()


class ExceptionHandler:
    context = _create_exception_handler_context_()

    @classmethod
    def register(cls, exceptions):
        class IllegalArguments(Exception): pass

        for e in exceptions:
            if not (issubclass(e[0], Exception) and issubclass(e[1], Exception)):
                raise IllegalArguments('Arguments are not Exception Type.')
            cls.context.register_exception(e[0], e[1])

    @classmethod
    def process(cls, exception, *args, group=None, handler=None, **kwargs):
        if not handler:
            handler = cls.context.handler(exception.__class__, group)

        if inspect.isfunction(handler):
            sig = inspect.signature(handler)

            arguments = []
            idx = 0
            for p in sig.parameters:
                if p in kwargs:
                    arguments.append(kwargs[p])
                elif p == 'exception' or issubclass(sig.parameters[p].annotation, Exception):
                    arguments.append(exception)
                elif args and len(args) > idx:
                    arguments.append(args[idx])
                    idx += 1
                else:
                    arguments.append(None)

            return handler(*arguments)
        else:
            converted = cls.context.convert(exception.__class__)
            if converted == exception.__class__:
                raise exception
            else:
                raise converted() from exception
