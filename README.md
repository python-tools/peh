# Python Exception Handler Support Tool

파이썬 예외처리 지원 라이브러리입니다.
PEH는 어플리케이션의 특정 루틴 수행중에 발생한 예외에 대한 예외 변환, 예외 처리 핸들러 등록 및 관리등을 지원합니다.

# 튜토리얼
## 예외처리 등록
PEH는 예외처리 장식자를 이용해 예외 처리기를 등록하고 지정 할 수 있습니다.
### Decorators
- 모듈 : peh.decorators

1. @exception_handler : 장식자가 붙은 함수를 예외처리기로 등록합니다.
2. @with_handler : 장식자가 붙은 함수를 실행할때 예외가 발생하면 등록된 예외 처리기로 예외 처리를 수행합니다.

예제
    
    from peh.decorators import exception_handler, with_handler
    
    @with_handler
    def my_function():
        raise MyException
        
    @exception_handler(MyException)
    def handler(exception):
        log.info('My Exception raised.')
        


### ExceptionHandler 및 Handler context
예외처리 장식자를 사용하지 않고 직접 예외처리기를 등록 할 수 있습니다.
- 모듈 : peh

예제

    from peh import ExceptionHadler
    
    def test_handler(exception):
        print('exception : ', type(exception))
    
    ExceptionHandler.context.handler(Exception, group=test_handler.__module__, handler=test_handler)

## ExceptionHandler With Django
PEH는 Django 프레임웍에과 통합하여 사용할 수 있습니다.
Django 프레임웍에서 PEH를 사용하기 위해서는 middleware 설정 및 핸들러 설정을 해야 합니다.
### Middleware 설정
- 모듈 : peh.django.middleware
