# setup.py
from setuptools import setup, find_packages


setup_requires = []

install_requires = []

dependency_links = []

setup(
    name='peh',
    version='1.0b7',
    description='Python Tools : Exception Handler',
    author='juphich',
    author_email='juphich@gmail.com',
    packages=find_packages(exclude=['*test*']),
    install_requires=install_requires,
    setup_requires=setup_requires,
    dependency_links=dependency_links,
)